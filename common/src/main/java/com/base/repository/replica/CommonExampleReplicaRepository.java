package com.base.repository.replica;

import com.base.model.Example;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Mapper
public interface CommonExampleReplicaRepository {

    @Transactional
    List<Example> findAllExample();

}
