package com.base.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.flywaydb.core.Flyway;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

@Configuration
@MapperScan(basePackages = {"com.base.repository.replica"}, sqlSessionFactoryRef = "sqlSessionFactoryReplica")
public class MyBatisConfigReplicaDB {
    @Bean(name = "dataSourceReplica")
    @ConfigurationProperties(prefix = "spring.datasource.replica")
    public DataSource dataSourceReplica() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "sqlSessionFactoryReplica")
    public SqlSessionFactory sqlSessionFactoryReplica() throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSourceReplica());
        factoryBean.setMapperLocations(new PathMatchingResourcePatternResolver()
                .getResources("classpath:mybatis/replica/*.xml"));
        return factoryBean.getObject();
    }

    @Bean(name = "transactionManagerReplica")
    public PlatformTransactionManager transactionManagerReplica() {
        return new DataSourceTransactionManager(dataSourceReplica());
    }

    @PostConstruct
    public void migrateFlyway() {
        Flyway flyway = Flyway.configure()
                .dataSource(dataSourceReplica())
                .locations("classpath:db/migration/replica")
                .load();
        flyway.migrate();
    }
}
