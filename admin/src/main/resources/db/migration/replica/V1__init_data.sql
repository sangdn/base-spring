CREATE TABLE IF NOT EXISTS `example` (
        `id` VARCHAR(36) PRIMARY KEY,
        `name` varchar(150) DEFAULT NULL,
        `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `deleted_at` timestamp DEFAULT NULL
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_general_ci;

INSERT INTO
    `example`
VALUES
    (
        '00000000-0000-0000-0000-000000000001',
        'name 1',
        '2023-09-05 18:29:40',
        '2023-09-05 18:29:40',
        NULL
    ),
    (
        '00000000-0000-0000-0000-000000000002',
        'name 2',
        '2023-09-05 18:29:40',
        '2023-09-05 18:29:40',
        NULL
    ),
    (
        '00000000-0000-0000-0000-000000000003',
        'name 3',
        '2023-09-05 18:29:40',
        '2023-09-05 18:29:40',
        NULL
    );