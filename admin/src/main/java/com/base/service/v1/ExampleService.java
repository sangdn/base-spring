package com.base.service.v1;

import com.base.model.Example;
import com.base.repository.replica.CommonExampleReplicaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ExampleService {
    private final CommonExampleReplicaRepository commonExampleReplicaRepository;

    public List<Example> getListExamples() {
        return commonExampleReplicaRepository.findAllExample();
    }
}
