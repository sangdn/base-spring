package com.base.api.v1.example;

import com.base.dto.v1.response.DataResponse;
import com.base.service.v1.ExampleService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/example")
@PreAuthorize("hasAuthority('ROLE_ADMIN')")
public class ExampleController {
    private final ExampleService exampleService;

    @ApiOperation("Get Examples list")
    @PostMapping("/list")
    public DataResponse getExamplesList() throws Exception {
        return DataResponse.okStatus(exampleService.getListExamples());
    }
}
